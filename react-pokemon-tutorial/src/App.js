import React, { useState, useEffect } from 'react';
import NavBar from './components/Navbar';
import Card from './components/Card';
import { getAllPokemon, getPokemon } from './services/pokemon';
import logo from './logo.svg';
import './App.css';

function App() {
    const [pokemonData, setPokemonData] = useState([]);
    const [nextUrl, setNextUrl] = useState('');
    const [prevUrl, setPrevUrl] = useState('');
    const [loading, setLoading] = useState(true);
    const initialUrl = 'https://pokeapi.co/api/v2/pokemon';


    const loadingPokemon = async (list) => {
        let _pokemonData = await Promise.all(list.map(async pokemon => {
            let pokemonRecord = await getPokemon(pokemon.url);
            return pokemonRecord;
        }));
        setPokemonData(_pokemonData);
    };

    const next = async () => {
        setLoading(true);
        let data = await getAllPokemon(nextUrl);
        await loadingPokemon(data.results);
        setNextUrl(data.next);
        setPrevUrl(data.previous);
        setLoading(false);
    }

    const prev = async () => {
        setLoading(true);
        let data = await getAllPokemon(prevUrl);
        await loadingPokemon(data.results);
        setNextUrl(data.next);
        setPrevUrl(data.previous);
        setLoading(false);
    }


    // load data on startup
    useEffect(() => {
        async function fetchData() {
            let response = await getAllPokemon(initialUrl);
            setNextUrl(response.next);
            setPrevUrl(response.previous);
            await loadingPokemon(response.results);
            setLoading(false);
        }
        fetchData();
    }, []);

    return (
        <div className="App">
            { loading ? <h1>Loading...</h1> : (
                <>
                    <NavBar/>
                    <div className="btns">
                        <button onClick={prev}>Previous</button>
                        <button onClick={next}>Next</button>
                    </div>
                    <div className="grid-container">
                        {
                            pokemonData.map(pokemon => {
                                return <Card key={pokemon.id} pokemon={pokemon}/>
                            })
                        }
                    </div>
                    <div className="btns">
                        <button onClick={prev}>Previous</button>
                        <button onClick={next}>Next</button>
                    </div>
                </>
            )}
        </div>
    );
}

export default App;
